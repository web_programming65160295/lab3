import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

interface AddressBook {
    id: number,
    name: string,
    tel: string,
    gender: string

}

export const useAddressBookStore = defineStore('address_book', () => {



    const address = ref<AddressBook>({
        id: 0,
        name: '',
        tel: '',
        gender: 'Male'
    })
    let lastID = 1
    const addressList = ref<AddressBook[]>([])
    const isAddNew = ref(false)
    function save() {
        if (address.value.id > 0) {
            const editedIndex = addressList.value.findIndex((item) => item.id === address.value.id)
            addressList.value[editedIndex] = address.value
        } else {
            addressList.value.push({ ...address.value, id: lastID++ })
            address.value = {
                id: 0,
                name: '',
                tel: '',
                gender: 'Male'
            }

        }
        isAddNew.value = false
    }

    function edit(id: number) {
        const editedIndex = addressList.value.findIndex((item) => item.id === id)
        address.value = JSON.parse(JSON.stringify(addressList.value[editedIndex]))
        isAddNew.value = true
    }


    function remove(id: number) {
        const removedIndex = addressList.value.findIndex((item) => item.id === id)
        addressList.value.splice(removedIndex, 1)
    }

    function cancel() {
        address.value = {
            id: 0,
            name: '',
            tel: '',
            gender: 'Male'
        }
        isAddNew.value = false
    }

    return { addressList, address, isAddNew, save, remove, cancel, edit }
})
